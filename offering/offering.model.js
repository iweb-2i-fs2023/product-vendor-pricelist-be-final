const offerings = [
  {
    id: '1',
    vendor: 'orellfuessli.ch',
    price: 45,
  },
  {
    id: '2',
    vendor: 'saxbooks.ch',
    price: 42,
  },
  {
    id: '3',
    vendor: 'fontis.ch',
    price: 53,
  },
];

function getAll() {
  return Promise.resolve(offerings);
}

function create(offering) {
  const newOffering = {
    id: getNextId(),
    vendor: offering.vendor,
    price: offering.price,
  };
  offerings.push(newOffering);
  return Promise.resolve(newOffering);
}

function getNextId() {
  if (!offerings || offerings.length === 0) {
    return '1';
  }
  return '' + (parseInt(offerings[offerings.length - 1].id) + 1);
}

function update(id, offering) {
  const existingOffering = findOffering(id);
  if (existingOffering) {
    existingOffering.vendor = offering.vendor;
    existingOffering.price = offering.price;
    return Promise.resolve(existingOffering);
  }
  return Promise.reject(`Failed to find offering with id ${id}`);
}

function remove(id) {
  const existingOffering = findOffering(id);
  if (existingOffering) {
    offerings.splice(offerings.indexOf(existingOffering), 1);
    return Promise.resolve(existingOffering);
  }
  return Promise.reject(`Failed to find offering with id ${id}`);
}

function findOffering(id) {
  const existingOfferings = offerings.filter((todo) => todo.id === id);
  if (existingOfferings && existingOfferings.length === 1) {
    return existingOfferings[0];
  }
  return null;
}

export { getAll, create, update, remove };
