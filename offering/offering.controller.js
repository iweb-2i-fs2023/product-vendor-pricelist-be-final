import { getAll, create, update, remove } from './offering.model.js';

async function getOfferings(request, response) {
  const offerings = await getAll();
  response.json(offerings);
}

async function createOffering(request, response) {
  const offering = await create(request.body);
  response.json(offering);
}

async function updateOffering(request, response) {
  await update(request.params.id, request.body)
    .then((offering) => {
      response.json(offering);
    })
    .catch((error) => {
      response.status(404).json({ message: error });
    });
}

async function removeOffering(request, response) {
  await remove(request.params.id)
    .then((offering) => {
      response.json(offering);
    })
    .catch((error) => {
      response.status(404).json({ message: error });
    });
}

export { getOfferings, createOffering, updateOffering, removeOffering };
