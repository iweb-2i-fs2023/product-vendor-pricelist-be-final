import express from 'express';
import { router as offeringRouter } from './offering/offering.routes.js';

const app = express();

app.use(express.json());
app.use('/api/offerings', offeringRouter);

app.listen(3001, () => {
  console.log('Server listens to http://localhost:3001');
});
